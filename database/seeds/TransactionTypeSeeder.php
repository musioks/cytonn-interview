<?php

use Illuminate\Database\Seeder;

class TransactionTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $names = [
            'Top-up',
            'Withdrawal'
        ];
        foreach ($names as $name) {
            \App\Transaction_type::create(
                ['name' => $name]
            );
        }
    }
}
