<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //DB::table('products')->truncate();
        $names = [
            'Direct equity',
            'Equity mutual funds',
            'Debt mutual funds',
            'National Pension System',
            'Public Provident Fund',
            'Bank fixed deposit',
            'Senior Citizens\' Saving Scheme',
            'RBI Taxable Bonds',
            'Real Estate',
            'Gold',
        ];
        $faker = Faker::create('App\Product');
        foreach ($names as $name) {
            DB::table('products')->insert(
                [
                    'name' => $name,
                    'price' => $faker->numberBetween($min = 1000, $max = 4000).'000',
                    'description' => $faker->text($maxNbChars = 200),
                    'created_at' => \Carbon\Carbon::now(),
                    'Updated_at' => \Carbon\Carbon::now(),
                ]
            );
        }
    }
}
