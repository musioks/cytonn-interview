<?php

use App\Client;
use App\Product;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ClientProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $products = Product::all();
        Client::chunk(10000, function ($clients) use ($products) {
                foreach ($clients as $client) {
                    foreach ($products->random(3) as $product) {
                        DB::table('client_products')->insert(
                            [
                                'client_id' => $client->id,
                                'product_id' => $product->id,
                                'created_at' => \Carbon\Carbon::now(),
                                'Updated_at' => \Carbon\Carbon::now(),
                            ]
                        );
                    }
                }
            }
        );
    }
}
