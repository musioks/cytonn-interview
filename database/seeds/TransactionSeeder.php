<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TransactionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $transaction_types = \App\Transaction_type::all();
        \App\Client_product::chunk(
            20000,
            function ($client_products) use ($transaction_types) {
                foreach ($client_products as $client_product) {
                    foreach ($transaction_types as $type) {
                        for ($i = 1; $i < 4; $i++) {
                            DB::table('transactions')->insert(
                                [
                                    'client_product_id' => $client_product->id,
                                    'transaction_type_id' => $type->id,
                                    'amount' => rand(50,600).'1000',
                                    'created_at' => \Carbon\Carbon::now(),
                                    'Updated_at' => \Carbon\Carbon::now(),
                                ]
                            );
                        }
                    }
                }
            }
        );
    }
}
