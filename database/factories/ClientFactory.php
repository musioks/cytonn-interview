<?php

/** @var Factory $factory */

use App\Client;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(Client::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email'=>$faker->unique()->safeEmail,
        'phone'=>$faker->unique()->phoneNumber,
        'address' => $faker->address,
    ];
});
