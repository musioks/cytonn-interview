@component('mail::message')
# Hello {{$client->name}}

The following is a list of your transactions.
<table>
    <thead>
    <tr>
        <th>Product</th>
        <th>Transaction Type</th>
        <th>Amount</th>
        <th>Date</th>
    </tr>
    </thead>
    <tbody>
    @foreach($data as $row)
    <tr>
        <td>{{$row->productName ?? ''}}</td>
        <td>{{$row->transactionType ?? ''}}</td>
        <td>{{$row->transactionAmount ?? ''}}</td>
        <td>{{$row->transactionDate ?? ''}}</td>
    </tr>
        @endforeach
    </tbody>
</table>


Thanks,<br>
{{ config('app.name') }}
@endcomponent
