@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Client Transactions</div>
                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                    <!-- ======== START TABLE === -->
                            @if(session()->has('success'))
                                <p>{{session()->get('success')}}</p>
                                @endif
                        <form action="{{route('send.statements')}}" method="POST">
                            @csrf
                            @method('GET')
                            <button type="submit" class="btn btn-primary btn-lg float-right mb-1">Send Statements</button>
                        </form>
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Product</th>
                                <th>Transaction Type</th>
                                <th>Amount</th>
                                <th>Transaction Date</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($transactions as $transaction)
                                <tr>
                                    <td>{{$loop->iteration}}</td>
                                    <td>{{$transaction->clientName}}</td>
                                    <td>{{$transaction->clientEmail}}</td>
                                    <td>{{$transaction->productName}}</td>
                                    <td>{{$transaction->transactionType}}</td>
                                    <td>{{$transaction->transactionAmount}}</td>
                                    <td>{{$transaction->transactionDate}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {{ $transactions->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
