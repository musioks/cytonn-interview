<?php

namespace App\Http\Controllers;

use App\Client;
use App\Mail\SendStatements;
use App\Transaction;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return Renderable
     */
    public function index()
    {
        ini_set('max_execution_time', 54000);
        $transactions = \App\Transaction::query()->get();
        $transactions->transform(
            function ($item, $key) {
                return [
                    'transactionId' => $item->id,
                    'clientId' => $item->clientProduct->client->id ?? '',
                    'clientName' => $item->clientProduct->client->name ?? '',
                    'clientEmail' => $item->clientProduct->client->email ?? '',
                    'productName' => $item->clientProduct->product->name ?? '',
                    'transactionType' => $item->transactionType->name ?? '',
                    'transactionAmount' => number_format($item->amount),
                    'transactionDate' => \Carbon\Carbon::parse($item->created_at)->format('d/m/Y H:i:s'),
                ];
            }
        );
        $transactions = collect(\App\Helpers\FlattenData::jsonFormat($transactions))->paginate(30);
        return view('home', compact('transactions'));
    }

    public function sendStatements(Request $request)
    {
        ini_set('max_execution_time', 54000);
        $transactions = Transaction::all()->transform(
            function ($value, $key) {
                return [
                    'id' => $value->id,
                    'clientId' => $value->clientProduct->client->id ?? '',
                    'clientName' => $value->clientProduct->client->name ?? '',
                    'clientEmail' => $value->clientProduct->client->email ?? '',
                    'productName' => $value->clientProduct->product->name ?? '',
                    'transactionType' => $value->transactionType->name ?? '',
                    'transactionAmount' => number_format($value->amount),
                    'transactionDate' => \Carbon\Carbon::parse($value->created_at)->format('d/m/Y H:i:s'),
                ];
            }
        );
        $transactions = collect(\App\Helpers\FlattenData::jsonFormat($transactions));
        Client::chunk(
            '10000',
            function ($clients) use ($transactions) {
                foreach ($clients as $client) {
                    $data = $transactions->where('clientId', $client->id);
                    $message = (new SendStatements($data, $client))
                        ->onQueue('emails_statements');
                    Mail::to($client->email)
                        ->queue($message);
                }
            }
        );
        return redirect()->back()->with('success', 'Statements have been sent!');
    }
}
