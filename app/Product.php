<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $guarded = [];

    public function clients()
    {
        return $this->belongsToMany(Client::class, 'client_products', 'product_id', 'client_id');
    }
}
