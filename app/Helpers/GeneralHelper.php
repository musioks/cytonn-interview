<?php


namespace App\Helpers;


use Illuminate\Support\Facades\DB;

class GeneralHelper
{
    //get transaction types

    /**
     * Top Up transaction type
     */
    public function topUp()
    {
        return DB::table('transaction_types')
            ->where('name', 'like', '%top-up%')->first();
    }

    /**
     * Withdrawal transaction type
     */
    public function withdrawal()
    {
        return DB::table('transaction_types')
            ->where('name', 'like', '%withdrawal%')->first();
    }

}
