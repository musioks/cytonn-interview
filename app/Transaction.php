<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $guarded = [];

    public function clientProduct()
    {
        return $this->belongsTo(Client_product::class,  'client_product_id');
    }
    public function transactionType()
    {
        return $this->belongsTo(Transaction_type::class,  'transaction_type_id');
    }

}
